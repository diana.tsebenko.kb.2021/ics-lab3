#!/bin/bash

dir="$1"

mkdir -p "$dir/documents" "$dir/images" "$dir/other"

total_docs=0
total_images=0
total=0
all_files="$dir/*"

for file in $all_files; do
  name_file=$(basename "$file")
  if [ -f "$file" ]; then
    extension="${file##*.}"
    case "$extension" in
      doc|pdf|txt)
        mv "$file" "$dir/documents/"
        total_docs=$((total_docs + 1))
        ;;
      jpg|png|gif|jpeg)
        mv "$file" "$dir/images/"
        total_images=$((total_images + 1))
        ;;
      *)
        mv "$file" "$dir/other/"
        total=$((total + 1))
        ;;
    esac
  fi
done

echo "$total_docs documents moved to $dir/documents"
echo "$total_images images moved to $dir/images"
echo "$total other files moved to $dir/other"